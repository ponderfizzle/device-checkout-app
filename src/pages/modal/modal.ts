import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ModalController } from 'ionic-angular';
import { FirebaseConfigService } from '../../app/core/service/firebase-config.service';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html',
})

export class ModalPage {
  deviceInfo: any;
  deviceId: string;
  userAction: string;
  assetNumber: string;
  borrower: string;
  deviceType: string;
  lastModified: number;
  name: string;
  note: string;
  osType: string;
  osVersion: string;
  owner: string;
  borrowerName: string = '';
  checkouts: number;
  action: string;
  deviceFound: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, private firebaseConfigService: FirebaseConfigService) { }

  ngOnInit() {
    this.userAction = this.navParams.get('userAction');
    this.assetNumber = this.navParams.get('selectedDevice');

    this.firebaseConfigService.getDevices(this.assetNumber).subscribe(devices => {
        if (devices.length == 0){
          this.deviceFound = false;
        }
        else {
          this.deviceFound = true;
          this.deviceInfo = devices;
          this.deviceId = this.deviceInfo[0]['$key']
          this.borrower = this.deviceInfo[0]['borrower'];
          this.deviceType = this.deviceInfo[0]['devicetype'];
          this.lastModified = this.deviceInfo[0]['lastmodified'];
          this.name = this.deviceInfo[0]['name'];
          this.note = this.deviceInfo[0]['note'];
          this.osType = this.deviceInfo[0]['ostype'];
          this.osVersion = this.deviceInfo[0]['osversion'];
          this.owner = this.deviceInfo[0]['owner'];
          this.checkouts = this.deviceInfo[0]['checkouts'];
          this.action = this.deviceInfo[0]['action'];
        }  
    })
  }

  checkoutDevice() {
    this.checkouts += 1;
    this.firebaseConfigService.updateDevice(this.deviceId, this.borrowerName, this.name, 'checked out', this.checkouts)
    this.viewCtrl.dismiss();
  }

  returnDevice() {
    this.firebaseConfigService.updateDevice(this.deviceId, this.borrower, this.name, 'returned', this.checkouts)
    this.viewCtrl.dismiss();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalPage');
  }

}
