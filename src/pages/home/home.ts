import { Component } from '@angular/core';
import { NavController, ModalController, NavParams } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { ModalPage } from '../modal/modal';
import { FirebaseConfigService } from '../../app/core/service/firebase-config.service'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {


  private device: any;

  constructor(public navCtrl: NavController, private barcodeScanner: BarcodeScanner, public modalCtrl: ModalController, private firebaseConfigService: FirebaseConfigService) { }

  ngOnInit() {
  }

  // Scanning devices will not work when testing in the browser.  To test in the browser uncomment the below section
  // and uncomment the button in the home.html file to enable a button that will directly open the checkout modal

  // testModal(userAction) {
  //     // valid device let myModal = this.modalCtrl.create(ModalPage, { 'userAction': userAction, 'selectedDevice': "023651" });
  //     // invalid devicelet myModal = this.modalCtrl.create(ModalPage, { 'userAction': userAction, 'selectedDevice': "123456" });
  //     myModal.present();
  // }

  changeDevice(userAction) {
    this.barcodeScanner.scan()
      .then((result) => {
        if (!result.cancelled) {
          let myModal = this.modalCtrl.create(ModalPage, { 'userAction': userAction, 'selectedDevice': result.text });
          myModal.present();
        }
      })
      .catch((err) => {
        alert("There was an error: " + err);
      })
  }
}
