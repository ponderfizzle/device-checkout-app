import { Injectable } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { FIREBASE_CONFIG } from '../constant/constants';

@Injectable()
export class FirebaseConfigService {
    devices: FirebaseListObservable<any[]>;
    constructor(private db: AngularFireDatabase) { }

    // Returns a list of devices as an Observable
    getDevices(assetNumber) {
        // console.log(this.devices);
        return this.devices = this.db.list('/devices/', {
            query: {
                orderByChild: 'assetnumber',
                equalTo: assetNumber
            }
        }) as FirebaseListObservable<Device[]>;
    }

    // Devices have a unique ID set by Firebase along with the unique asset # given by the IS dept.
    updateDevice(id: string, borrower: string, name: string, action: string, checkouts?: number) {
        var returningUser = borrower;
        if (action === "returned"){
            borrower = "";
        }
        const history = {name: name, borrower: returningUser, action: action, lastmodified: Date.now()};
        const devices = this.db.list('/devices/');

        this.devices.update(id, { borrower: borrower, action: action, lastmodified: Date.now(), checkouts: checkouts })
        this.db.database.ref('/history/').push(history);
    }
}

interface Device {
    $key?: string,
    assetnumber?: number,
    borrower?: string,
    devicetype?: string,
    lastmodified?: number,
    name?: string,
    note?: string,
    ostype?: string,
    action?: string,
    osversion?: string,
    owner?: string,
    checkouts?: number
}

interface History {
    $key?: string,
    name: string,
    action?: string,
    borrower?: string,
    lastmodified?: number
}